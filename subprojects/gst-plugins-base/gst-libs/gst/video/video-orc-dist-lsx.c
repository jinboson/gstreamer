/* GStreamer
 * Copyright (C) <2022> Jin Bo <jinbo@loongson.cn>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "video-orc-dist-lsx.h"

#if defined(__loongarch_sx)

#include <lsxintrin.h>

#define ORC_CLAMP(x,a,b) ((x)<(a) ? (a) : ((x)>(b) ? (b) : (x)))
#define ORC_SB_MAX 127
#define ORC_SB_MIN (-1-ORC_SB_MAX)
#define ORC_CLAMP_SB(x) ORC_CLAMP(x,ORC_SB_MIN,ORC_SB_MAX)
#define ORC_PTR_OFFSET(ptr,offset) ((void *)(((unsigned char *)(ptr)) + (offset)))
#define ORC_SWAP_L(x) ((((x)&0xffU)<<24) | (((x)&0xff00U)<<8) | (((x)&0xff0000U)>>8) | (((x)&0xff000000U)>>24))

void
video_orc_convert_I420_BGRA (guint8 * ORC_RESTRICT d1,
    const guint8 * ORC_RESTRICT s1, const guint8 * ORC_RESTRICT s2,
    const guint8 * ORC_RESTRICT s3, int p1, int p2, int p3, int p4, int p5,
    int n)
{
  int i, j;
  orc_union32 *ORC_RESTRICT ptr0;
  const orc_int8 *ORC_RESTRICT ptr4;
  const orc_int8 *ORC_RESTRICT ptr5;
  const orc_int8 *ORC_RESTRICT ptr6;
  orc_int8 var42;
  orc_int8 var43;
  orc_union16 var44;
  orc_union16 var45;
  orc_int8 var46;
  orc_union16 var47;
  orc_union16 var48;
  orc_union16 var49;
  orc_union32 var50;
  orc_union32 var51;
  orc_int8 var52;
  orc_union16 var53;
  orc_int8 var54;
  orc_int8 var55;
  orc_union16 var56;
  orc_int8 var57;
  orc_int8 var58;
  orc_union16 var59;
  orc_union16 var60;
  orc_union16 var61;
  orc_union16 var62;
  orc_int8 var63;
  orc_union16 var64;
  orc_union16 var65;
  orc_union16 var66;
  orc_int8 var67;
  orc_union16 var68;
  orc_union16 var69;
  orc_union16 var70;
  orc_union16 var71;
  orc_int8 var72;
  orc_union16 var73;
  orc_union32 var74;

  __m128i v42, v43, v46, v52, v54, v55, v57, v58, v63, v67, v72, v0x80;
  __m128i v44_l, v45_l, v47_l, v48_l, v49_l, v53_l, v53_h, v56_l, v56_h;
  __m128i v59_l, v59_h, v60_l, v60_h, v61_l, v61_h, v62_l, v62_h, v64_l;
  __m128i v64_h, v65_l, v65_h, v66_l, v66_h, v68_l, v68_h, v69_l;
  __m128i v69_h, v70_l, v70_h, v71_l, v71_h, v73_l, v73_h;
  __m128i tmp1_l, tmp1_h, tmp2_l, tmp2_h;

  v43 = __lsx_vreplgr2vr_b (0x80);
  v44_l = __lsx_vreplgr2vr_h (p1);
  v45_l = __lsx_vreplgr2vr_h (p2);
  v46 = __lsx_vreplgr2vr_b (0x7f);
  v47_l = __lsx_vreplgr2vr_h (p3);
  v48_l = __lsx_vreplgr2vr_h (p4);
  v49_l = __lsx_vreplgr2vr_h (p5);
  v0x80 = __lsx_vreplgr2vr_b (0x80);

  ptr0 = (orc_union32 *) d1;
  ptr4 = (orc_int8 *) s1;
  ptr5 = (orc_int8 *) s2;
  ptr6 = (orc_int8 *) s3;

  /* 1: loadpb */
  var43 = 0x00000080;           /* 128 or 6.32404e-322f */
  /* 10: loadpw */
  var44.i = p1;
  /* 12: loadpw */
  var45.i = p2;
  /* 16: loadpb */
  var46 = 0x0000007f;           /* 127 or 6.27463e-322f */
  /* 18: loadpw */
  var47.i = p3;
  /* 22: loadpw */
  var48.i = p4;
  /* 25: loadpw */
  var49.i = p5;
  /* 31: loadpb */
  var50.x4[0] = 0x00000080;     /* 128 or 6.32404e-322f */
  var50.x4[1] = 0x00000080;     /* 128 or 6.32404e-322f */
  var50.x4[2] = 0x00000080;     /* 128 or 6.32404e-322f */
  var50.x4[3] = 0x00000080;     /* 128 or 6.32404e-322f */

  j = n & -16;
  for (i = 0; i < j; i += 16) {
    v42 = __lsx_vld (ptr4, 0);
    v52 = __lsx_vsub_b (v42, v43);
    v53_l = __lsx_vilvl_b (v52, v52);
    v53_h = __lsx_vilvh_b (v52, v52);
    v54 = __lsx_vld (ptr5, 0);
    v54 = __lsx_vilvl_b (v54, v54);
    v55 = __lsx_vsub_b (v54, v43);
    v56_l = __lsx_vilvl_b (v55, v55);
    v56_h = __lsx_vilvh_b (v55, v55);
    v57 = __lsx_vld (ptr6, 0);
    v57 = __lsx_vilvl_b (v57, v57);
    v58 = __lsx_vsub_b (v57, v43);
    v59_l = __lsx_vilvl_b (v58, v58);
    v59_h = __lsx_vilvh_b (v58, v58);
    v60_l = __lsx_vmuh_h (v53_l, v44_l);
    v60_h = __lsx_vmuh_h (v53_h, v44_l);
    v61_l = __lsx_vmuh_h (v59_l, v45_l);
    v61_h = __lsx_vmuh_h (v59_h, v45_l);
    v62_l = __lsx_vadd_h (v60_l, v61_l);
    v62_h = __lsx_vadd_h (v60_h, v61_h);
    v62_l = __lsx_vsat_h (v62_l, 7);
    v62_h = __lsx_vsat_h (v62_h, 7);
    v63 = __lsx_vpickev_b (v62_h, v62_l);
    v64_l = __lsx_vilvl_b (v46, v63);
    v64_h = __lsx_vilvh_b (v46, v63);
    v65_l = __lsx_vmuh_h (v56_l, v47_l);
    v65_h = __lsx_vmuh_h (v56_h, v47_l);
    v66_l = __lsx_vadd_h (v60_l, v65_l);
    v66_h = __lsx_vadd_h (v60_h, v65_h);
    v66_l = __lsx_vsat_h (v66_l, 7);
    v66_h = __lsx_vsat_h (v66_h, 7);
    v67 = __lsx_vpickev_b (v66_h, v66_l);
    v68_l = __lsx_vmuh_h (v56_l, v48_l);
    v68_h = __lsx_vmuh_h (v56_h, v48_l);
    v69_l = __lsx_vadd_h (v60_l, v68_l);
    v69_h = __lsx_vadd_h (v60_h, v68_h);
    v70_l = __lsx_vmuh_h (v59_l, v49_l);
    v70_h = __lsx_vmuh_h (v59_h, v49_l);
    v71_l = __lsx_vadd_h (v69_l, v70_l);
    v71_h = __lsx_vadd_h (v69_h, v70_h);
    v71_l = __lsx_vsat_h (v71_l, 7);
    v71_h = __lsx_vsat_h (v71_h, 7);
    v72 = __lsx_vpickev_b (v71_h, v71_l);
    v73_l = __lsx_vilvl_b (v72, v67);
    v73_h = __lsx_vilvh_b (v72, v67);
    tmp1_l = __lsx_vilvl_h (v64_l, v73_l);
    tmp1_h = __lsx_vilvh_h (v64_l, v73_l);
    tmp2_l = __lsx_vilvl_h (v64_h, v73_h);
    tmp2_h = __lsx_vilvh_h (v64_h, v73_h);
    tmp1_l = __lsx_vadd_b (tmp1_l, v0x80);
    tmp1_h = __lsx_vadd_b (tmp1_h, v0x80);
    tmp2_l = __lsx_vadd_b (tmp2_l, v0x80);
    tmp2_h = __lsx_vadd_b (tmp2_h, v0x80);
    __lsx_vstx (tmp1_l, ptr0, 0);
    __lsx_vstx (tmp1_h, ptr0, 16);
    __lsx_vstx (tmp2_l, ptr0, 32);
    __lsx_vstx (tmp2_h, ptr0, 48);
    ptr0 += 16;
    ptr4 += 16;
    ptr5 += 8;
    ptr6 += 8;
  }
  for (i = 0; i < (n & 15); i++) {
    /* 0: loadb */
    var42 = ptr4[i];
    /* 2: subb */
    var52 = var42 - var43;
    /* 3: splatbw */
    var53.i = ((var52 & 0xff) << 8) | (var52 & 0xff);
    /* 4: loadupdb */
    var54 = ptr5[i >> 1];
    /* 5: subb */
    var55 = var54 - var43;
    /* 6: splatbw */
    var56.i = ((var55 & 0xff) << 8) | (var55 & 0xff);
    /* 7: loadupdb */
    var57 = ptr6[i >> 1];
    /* 8: subb */
    var58 = var57 - var43;
    /* 9: splatbw */
    var59.i = ((var58 & 0xff) << 8) | (var58 & 0xff);
    /* 11: mulhsw */
    var60.i = (var53.i * var44.i) >> 16;
    /* 13: mulhsw */
    var61.i = (var59.i * var45.i) >> 16;
    /* 14: addw */
    var62.i = var60.i + var61.i;
    /* 15: convssswb */
    var63 = ORC_CLAMP_SB (var62.i);
    /* 17: mergebw */
    {
      orc_union16 _dest;
      _dest.x2[0] = var63;
      _dest.x2[1] = var46;
      var64.i = _dest.i;
    }
    /* 19: mulhsw */
    var65.i = (var56.i * var47.i) >> 16;
    /* 20: addw */
    var66.i = var60.i + var65.i;
    /* 21: convssswb */
    var67 = ORC_CLAMP_SB (var66.i);
    /* 23: mulhsw */
    var68.i = (var56.i * var48.i) >> 16;
    /* 24: addw */
    var69.i = var60.i + var68.i;
    /* 26: mulhsw */
    var70.i = (var59.i * var49.i) >> 16;
    /* 27: addw */
    var71.i = var69.i + var70.i;
    /* 28: convssswb */
    var72 = ORC_CLAMP_SB (var71.i);
    /* 29: mergebw */
    {
      orc_union16 _dest;
      _dest.x2[0] = var67;
      _dest.x2[1] = var72;
      var73.i = _dest.i;
    }
    /* 30: mergewl */
    {
      orc_union32 _dest;
      _dest.x2[0] = var73.i;
      _dest.x2[1] = var64.i;
      var74.i = _dest.i;
    }
    /* 32: addb */
    var51.x4[0] = var74.x4[0] + var50.x4[0];
    var51.x4[1] = var74.x4[1] + var50.x4[1];
    var51.x4[2] = var74.x4[2] + var50.x4[2];
    var51.x4[3] = var74.x4[3] + var50.x4[3];
    /* 33: storel */
    ptr0[i] = var51;
  }
}

void
video_orc_convert_AYUV_ARGB (guint8 * ORC_RESTRICT d1, int d1_stride,
    const guint8 * ORC_RESTRICT s1, int s1_stride, int p1, int p2, int p3,
    int p4, int p5, int n, int m)
{
  int i;
  int j;
  orc_union32 *ORC_RESTRICT ptr0;
  const orc_union32 *ORC_RESTRICT ptr4;
  orc_union32 var46;
  orc_union32 var47;
  orc_union16 var48;
  orc_union16 var49;
  orc_union16 var50;
  orc_union16 var51;
  orc_union16 var52;
  orc_union32 var53;
  orc_union32 var54;
  orc_union16 var55;
  orc_union16 var56;
  orc_int8 var57;
  orc_int8 var58;
  orc_int8 var59;
  orc_int8 var60;
  orc_union16 var61;
  orc_union16 var62;
  orc_union16 var63;
  orc_union16 var64;
  orc_union16 var65;
  orc_union16 var66;
  orc_int8 var67;
  orc_union16 var68;
  orc_union16 var69;
  orc_union16 var70;
  orc_int8 var71;
  orc_union16 var72;
  orc_union16 var73;
  orc_union16 var74;
  orc_union16 var75;
  orc_int8 var76;
  orc_union16 var77;
  orc_union32 var78;

  __m128i v46, v47, v48, v49, v50, v51, v52, v54, v55, v56, v57, v58;
  __m128i v59, v60, v61, v62, v63, v64, v65, v66, v67, v68, v69, v70;
  __m128i v71, v72, v73, v74, v75, v76, v77, v78, zero;

  /* 1: loadpb */
  var47.x4[0] = 0x00000080;     /* 128 or 6.32404e-322f */
  var47.x4[1] = 0x00000080;     /* 128 or 6.32404e-322f */
  var47.x4[2] = 0x00000080;     /* 128 or 6.32404e-322f */
  var47.x4[3] = 0x00000080;     /* 128 or 6.32404e-322f */
  /* 9: loadpw */
  var48.i = p1;
  /* 11: loadpw */
  var49.i = p2;
  /* 16: loadpw */
  var50.i = p3;
  /* 20: loadpw */
  var51.i = p4;
  /* 23: loadpw */
  var52.i = p5;

  v47 = __lsx_vreplgr2vr_b (0x80);
  v48 = __lsx_vreplgr2vr_h (p1);
  v49 = __lsx_vreplgr2vr_h (p2);
  v50 = __lsx_vreplgr2vr_h (p3);
  v51 = __lsx_vreplgr2vr_h (p4);
  v52 = __lsx_vreplgr2vr_h (p5);
  zero = __lsx_vreplgr2vr_b (0);

  for (j = 0; j < m; j++) {
    ptr0 = ORC_PTR_OFFSET (d1, d1_stride * j);
    ptr4 = ORC_PTR_OFFSET (s1, s1_stride * j);

    for (i = 0; i < (n & -4); i += 4) {
      v46 = __lsx_vld (ptr4, 0);
      v54 = __lsx_vsub_b (v46, v47);
      v55 = __lsx_vpickod_h (zero, v54);
      v56 = __lsx_vpickev_h (zero, v54);
      v57 = __lsx_vpickod_b (zero, v56);
      v58 = __lsx_vpickev_b (zero, v56);
      v59 = __lsx_vpickod_b (zero, v55);
      v60 = __lsx_vpickev_b (zero, v55);
      v61 = __lsx_vilvl_b (v57, v57);
      v62 = __lsx_vilvl_b (v60, v60);
      v63 = __lsx_vilvl_b (v59, v59);
      v64 = __lsx_vmuh_h (v61, v48);
      v65 = __lsx_vmuh_h (v63, v49);
      v66 = __lsx_vadd_h (v64, v65);
      v66 = __lsx_vsat_h (v66, 7);
      v67 = __lsx_vpickev_b (zero, v66);
      v68 = __lsx_vilvl_b (v67, v58);
      v69 = __lsx_vmuh_h (v62, v50);
      v70 = __lsx_vadd_h (v64, v69);
      v70 = __lsx_vsat_h (v70, 7);
      v71 = __lsx_vpickev_b (zero, v70);
      v72 = __lsx_vmuh_h (v62, v51);
      v73 = __lsx_vadd_h (v64, v72);
      v74 = __lsx_vmuh_h (v63, v52);
      v75 = __lsx_vadd_h (v73, v74);
      v75 = __lsx_vsat_h (v75, 7);
      v76 = __lsx_vpickev_b (zero, v75);
      v77 = __lsx_vilvl_b (v71, v76);
      v78 = __lsx_vilvl_h (v77, v68);
      v78 = __lsx_vadd_b (v78, v47);
      __lsx_vst (v78, ptr0, 0);
      ptr0 += 4;
      ptr4 += 4;
    }
    for (i = 0; i < (n & 3); i++) {
      /* 0: loadl */
      var46 = ptr4[i];
      /* 2: subb */
      var54.x4[0] = var46.x4[0] - var47.x4[0];
      var54.x4[1] = var46.x4[1] - var47.x4[1];
      var54.x4[2] = var46.x4[2] - var47.x4[2];
      var54.x4[3] = var46.x4[3] - var47.x4[3];
      /* 3: splitlw */
      {
        orc_union32 _src;
        _src.i = var54.i;
        var55.i = _src.x2[1];
        var56.i = _src.x2[0];
      }
      /* 4: splitwb */
      {
        orc_union16 _src;
        _src.i = var56.i;
        var57 = _src.x2[1];
        var58 = _src.x2[0];
      }
      /* 5: splitwb */
      {
        orc_union16 _src;
        _src.i = var55.i;
        var59 = _src.x2[1];
        var60 = _src.x2[0];
      }
      /* 6: splatbw */
      var61.i = ((var57 & 0xff) << 8) | (var57 & 0xff);
      /* 7: splatbw */
      var62.i = ((var60 & 0xff) << 8) | (var60 & 0xff);
      /* 8: splatbw */
      var63.i = ((var59 & 0xff) << 8) | (var59 & 0xff);
      /* 10: mulhsw */
      var64.i = (var61.i * var48.i) >> 16;
      /* 12: mulhsw */
      var65.i = (var63.i * var49.i) >> 16;
      /* 13: addw */
      var66.i = var64.i + var65.i;
      /* 14: convssswb */
      var67 = ORC_CLAMP_SB (var66.i);
      /* 15: mergebw */
      {
        orc_union16 _dest;
        _dest.x2[0] = var58;
        _dest.x2[1] = var67;
        var68.i = _dest.i;
      }
      /* 17: mulhsw */
      var69.i = (var62.i * var50.i) >> 16;
      /* 18: addw */
      var70.i = var64.i + var69.i;
      /* 19: convssswb */
      var71 = ORC_CLAMP_SB (var70.i);
      /* 21: mulhsw */
      var72.i = (var62.i * var51.i) >> 16;
      /* 22: addw */
      var73.i = var64.i + var72.i;
      /* 24: mulhsw */
      var74.i = (var63.i * var52.i) >> 16;
      /* 25: addw */
      var75.i = var73.i + var74.i;
      /* 26: convssswb */
      var76 = ORC_CLAMP_SB (var75.i);
      /* 27: mergebw */
      {
        orc_union16 _dest;
        _dest.x2[0] = var76;
        _dest.x2[1] = var71;
        var77.i = _dest.i;
      }
      /* 28: mergewl */
      {
        orc_union32 _dest;
        _dest.x2[0] = var68.i;
        _dest.x2[1] = var77.i;
        var78.i = _dest.i;
      }
      /* 29: addb */
      var53.x4[0] = var78.x4[0] + var47.x4[0];
      var53.x4[1] = var78.x4[1] + var47.x4[1];
      var53.x4[2] = var78.x4[2] + var47.x4[2];
      var53.x4[3] = var78.x4[3] + var47.x4[3];
      /* 30: storel */
      ptr0[i] = var53;
    }
  }
}

void
video_orc_unpack_Y444 (guint8 * ORC_RESTRICT d1, const guint8 * ORC_RESTRICT s1,
    const guint8 * ORC_RESTRICT s2, const guint8 * ORC_RESTRICT s3, int n)
{
  int i;
  orc_union32 *ORC_RESTRICT ptr0;
  const orc_int8 *ORC_RESTRICT ptr4;
  const orc_int8 *ORC_RESTRICT ptr5;
  const orc_int8 *ORC_RESTRICT ptr6;
  orc_int8 var34;
  orc_int8 var35;
  orc_int8 var36;
  orc_int8 var37;
  orc_union32 var38;
  orc_union16 var39;
  orc_union16 var40;

  __m128i v34, v35, v36, v37;
  __m128i v39_l, v39_h, v40_l, v40_h;
  __m128i v38_0, v38_1, v38_2, v38_3;

  ptr0 = (orc_union32 *) d1;
  ptr4 = (orc_int8 *) s1;
  ptr5 = (orc_int8 *) s2;
  ptr6 = (orc_int8 *) s3;

  /* 3: loadpb */
  var36 = 0x000000ff;           /* 255 or 1.25987e-321f */

  v36 = __lsx_vreplgr2vr_b (0xff);

  for (i = 0; i < (n & -16); i += 16) {
    v34 = __lsx_vld (ptr5, 0);
    v35 = __lsx_vld (ptr6, 0);
    v37 = __lsx_vld (ptr4, 0);
    v39_l = __lsx_vilvl_b (v35, v34);
    v39_h = __lsx_vilvh_b (v35, v34);
    v40_l = __lsx_vilvl_b (v37, v36);
    v40_h = __lsx_vilvh_b (v37, v36);
    v38_0 = __lsx_vilvl_h (v39_l, v40_l);
    v38_1 = __lsx_vilvh_h (v39_l, v40_l);
    v38_2 = __lsx_vilvl_h (v39_h, v40_h);
    v38_3 = __lsx_vilvh_h (v39_h, v40_h);
    __lsx_vst (v38_0, ptr0, 0);
    __lsx_vst (v38_1, ptr0, 16);
    __lsx_vst (v38_2, ptr0, 32);
    __lsx_vst (v38_3, ptr0, 48);
    ptr0 += 16;
    ptr4 += 16;
    ptr5 += 16;
    ptr6 += 16;
  }
  for (i = 0; i < (n & 15); i++) {
    /* 0: loadb */
    var34 = ptr5[i];
    /* 1: loadb */
    var35 = ptr6[i];
    /* 2: mergebw */
    {
      orc_union16 _dest;
      _dest.x2[0] = var34;
      _dest.x2[1] = var35;
      var39.i = _dest.i;
    }
    /* 4: loadb */
    var37 = ptr4[i];
    /* 5: mergebw */
    {
      orc_union16 _dest;
      _dest.x2[0] = var36;
      _dest.x2[1] = var37;
      var40.i = _dest.i;
    }
    /* 6: mergewl */
    {
      orc_union32 _dest;
      _dest.x2[0] = var40.i;
      _dest.x2[1] = var39.i;
      var38.i = _dest.i;
    }
    /* 7: storel */
    ptr0[i] = var38;
  }
}

void
video_orc_pack_BGRA (guint8 * ORC_RESTRICT d1, const guint8 * ORC_RESTRICT s1,
    int n)
{
  int i;
  orc_union32 *ORC_RESTRICT ptr0;
  const orc_union32 *ORC_RESTRICT ptr4;
  orc_union32 var32;
  orc_union32 var33;

  __m128i v32, v33;

  ptr0 = (orc_union32 *) d1;
  ptr4 = (orc_union32 *) s1;

  for (i = 0; i < (n & -4); i += 4) {
    v32 = __lsx_vld (ptr4, 0);
    v33 = __lsx_vshuf4i_b (v32, 0x1b);
    __lsx_vst (v33, ptr0, 0);
    ptr4 += 4;
    ptr0 += 4;
  }
  for (i = 0; i < (n & 3); i++) {
    /* 0: loadl */
    var32 = ptr4[i];
    /* 1: swapl */
    var33.i = ORC_SWAP_L (var32.i);
    /* 2: storel */
    ptr0[i] = var33;
  }
}

#endif
